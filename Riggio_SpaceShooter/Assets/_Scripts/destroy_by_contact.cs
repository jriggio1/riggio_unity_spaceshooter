﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroy_by_contact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    private Game_controller gameController;
    public int scoreValue;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent <Game_controller>(); 
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }
    void OnTriggerEnter(Collider other) {
        if (other.tag == "Boundary")
        {
            return;
        }
        Instantiate(explosion, transform.position, transform.rotation);
        if (other.tag == "Player") {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }
        gameController.Addscore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
